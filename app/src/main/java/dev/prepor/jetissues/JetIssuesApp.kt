package dev.prepor.jetissues

import androidx.compose.Composable
import androidx.ui.material.MaterialTheme
import dev.prepor.jetissues.data.State
import dev.prepor.jetissues.ui.IssueUI

typealias EffectHandler = (Event) -> Unit

@Composable
fun JetIssuesApp(state: State, effect: EffectHandler) {
    MaterialTheme(
        colors = lightThemeColors
    ) {
        AppContent(state, effect)
    }
}

@Composable
fun AppContent(state: State, effect: EffectHandler) {
    state.currentIssue?.let {
        IssueUI.Full(effect, it)
    } ?: run {
        IssueUI.List(state, effect)
    }
}

