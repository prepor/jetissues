package dev.prepor.jetissues.data

import androidx.compose.*
import androidx.compose.frames.ModelList
import dev.prepor.jetissues.IssuesQuery
import dev.prepor.jetissues.type.IssueState
import dev.prepor.jetissues.type.OrderDirection

enum class IssuesState {
    OPEN {
        override fun title() = "Open"
        override fun githubState() = IssueState.OPEN
    },
    CLOSED {
        override fun title() = "Closed"
        override fun githubState() = IssueState.CLOSED
    };

    abstract fun title(): String
    abstract fun githubState(): IssueState
}

enum class IssuesOrder {
    DESC {
        override fun githubOrder() = OrderDirection.DESC
    },
    ASC {
        override fun githubOrder() = OrderDirection.ASC
    };

    abstract fun githubOrder(): OrderDirection
}

@Model
class State {
    var loading: Boolean = false
    var issues: ModelList<Issue> = ModelList()
    var loadNext: (() -> Unit)? = null
    var currentIssue: Issue? = null
    var error: String? = null
    var issuesState: IssuesState = IssuesState.OPEN
    var issuesOrder: IssuesOrder = IssuesOrder.DESC
}

@Model
data class Issue(val github: IssuesQuery.Node, var text: String? = null, var error: String? = null)