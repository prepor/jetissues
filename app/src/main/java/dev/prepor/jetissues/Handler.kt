package dev.prepor.jetissues

import android.os.Handler
import android.os.Looper
import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.response.CustomTypeAdapter
import com.apollographql.apollo.response.CustomTypeValue
import dev.prepor.jetissues.data.Issue
import dev.prepor.jetissues.data.IssuesOrder
import dev.prepor.jetissues.data.IssuesState
import dev.prepor.jetissues.data.State
import dev.prepor.jetissues.type.CustomType
import okhttp3.OkHttpClient
import java.lang.UnsupportedOperationException
import java.time.Instant
import java.util.*

sealed class Event {
    object Refresh : Event()
    data class IssuesLoaded(val issues: IssuesQuery.Issues, val replace: Boolean) : Event()
    data class SelectIssue(val issue: Issue) : Event()
    object MainScreen : Event()
    data class LoadError(val e: Exception) : Event()
    data class SetIssuesState(val state: IssuesState) : Event()
    data class SetIssuesOrder(val order: IssuesOrder) : Event()
    data class IssueLoaded(val issue: Issue, val text: String) : Event()
    data class IssueLoadError(val issue: Issue, val e: Exception) : Event()
}

var mainHandler = Handler(Looper.getMainLooper())

class Effects(val state: State) {
    private val TAG = "Handler"
    private val OWNER = BuildConfig.GITHUB_OWNER
    private val REPO = BuildConfig.GITHUB_REPO

    class UknownRepo : RuntimeException()
    class UnknownIssue : RuntimeException()

    private fun loadIssues(cursor: String?) {
        Log.i(TAG, "Loading issues")
        state.loading = true
        state.error = null
        val query = IssuesQuery(OWNER, REPO,
            after = Input.optional(cursor),
            state = state.issuesState.githubState(),
            direction = state.issuesOrder.githubOrder())
        val callback = object : ApolloCall.Callback<IssuesQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                schedule(Event.LoadError(e))
            }

            override fun onResponse(response: Response<IssuesQuery.Data>) {
                val repo = response.data()?.repository
                if (repo == null) {
                    schedule(Event.LoadError(UknownRepo()))
                } else {
                    schedule(Event.IssuesLoaded(repo.issues, cursor == null))
                }
            }
        }
        apolloClient.query(query).enqueue(callback)
    }

    private fun loadIssue(issue: Issue) {
        Log.i(TAG, "Loading issue ${issue.github.number}")
        issue.error = null
        val query = IssueQuery(OWNER, REPO, issue.github.number)
        val callback = object : ApolloCall.Callback<IssueQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                schedule(Event.LoadError(e))
            }

            override fun onResponse(response: Response<IssueQuery.Data>) {
                val text = response.data()?.repository?.issue?.body
                if (text == null) {
                    schedule(Event.IssueLoadError(issue, UnknownIssue()))
                } else {
                    schedule(Event.IssueLoaded(issue, text))
                }
            }
        }
        apolloClient.query(query).enqueue(callback)
    }

    private fun handle(e: Event) {
        when (e) {
            is Event.Refresh -> {
                state.issues.clear()
                loadIssues(null)
            }
            is Event.IssuesLoaded -> {
                Log.i(TAG, "Loaded!")
                state.loading = false
                if (e.replace) {
                    state.issues.clear()
                }
                state.issues.addAll(e.issues.nodes!!.map {
                    Issue(
                        it!!
                    )
                })
                e.issues.pageInfo.endCursor?.let {
                    state.loadNext = {
                        loadIssues(it)
                    }
                } ?: run {
                    state.loadNext = null
                }
            }
            is Event.SelectIssue -> {
                state.currentIssue = e.issue
                loadIssue(e.issue)
            }
            is Event.LoadError -> {
                Log.e(TAG, "Loading error", e.e)
                state.error = e.e.toString()
            }
            is Event.MainScreen -> {
                state.currentIssue = null
            }
            is Event.SetIssuesState -> {
                state.issues.clear()
                state.issuesState = e.state
                loadIssues(null)
            }
            is Event.SetIssuesOrder -> {
                state.issues.clear()
                state.issuesOrder = e.order
                loadIssues(null)
            }
            is Event.IssueLoaded -> {
                e.issue.text = e.text
            }
            is Event.IssueLoadError -> {
                Log.e(TAG, "Loading error", e.e)
                e.issue.error = e.e.toString()
            }
        }
    }

    fun schedule(e: Event) {
        mainHandler.post{ handle(e) }
    }
}

private const val baseUrl = "https://api.github.com/graphql"
private val apolloClient: ApolloClient by lazy {
    val okHttpClient = OkHttpClient.Builder()
        .addNetworkInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "bearer ${BuildConfig.GITHUB_OAUTH_TOKEN}")
                .build()

            chain.proceed(request)
        }
        .build()

    ApolloClient.builder()
        .serverUrl(baseUrl)
        .addCustomTypeAdapter(CustomType.DATETIME, object : CustomTypeAdapter<Date> {
            override fun encode(value: Date): CustomTypeValue<*> {
                throw UnsupportedOperationException()
            }

            override fun decode(value: CustomTypeValue<*>): Date {
                val v = value.value
                if (v is String) {
                    return Date.from(Instant.parse(v))
                }
                throw IllegalArgumentException(value.toString())
            }
        })
        .okHttpClient(okHttpClient)
        .build()
}