package dev.prepor.jetissues.ui

import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Text
import androidx.ui.foundation.Box
import androidx.ui.layout.LayoutPadding
import androidx.ui.layout.LayoutWidth
import androidx.ui.material.CircularProgressIndicator
import androidx.ui.material.MaterialTheme
import androidx.ui.text.TextStyle
import androidx.ui.text.font.FontWeight
import androidx.ui.unit.dp

@Composable
fun Loader() {
    Box(gravity = Alignment.Center, modifier = LayoutWidth.Fill + LayoutPadding(20.dp)) {
        CircularProgressIndicator()
    }
}

@Composable
fun Error(err: String) {
    Box(gravity = Alignment.Center, modifier = LayoutWidth.Fill + LayoutPadding(20.dp)) {
        Text(text = err, style = TextStyle(color = MaterialTheme.colors().error, fontWeight = FontWeight.Bold))
    }
}
