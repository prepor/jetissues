package dev.prepor.jetissues.ui

import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Text
import androidx.ui.foundation.Box
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.ScrollerPosition
import androidx.ui.foundation.VerticalScroller
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.graphics.luminance
import androidx.ui.layout.*
import androidx.ui.material.*
import androidx.ui.material.ripple.Ripple
import androidx.ui.material.surface.Card
import androidx.ui.text.AnnotatedString
import androidx.ui.text.SpanStyle
import androidx.ui.text.TextStyle
import androidx.ui.text.font.FontStyle
import androidx.ui.text.font.FontWeight
import androidx.ui.unit.dp
import dev.prepor.jetissues.*
import dev.prepor.jetissues.R
import dev.prepor.jetissues.data.Issue
import dev.prepor.jetissues.data.IssuesOrder
import dev.prepor.jetissues.data.IssuesState
import dev.prepor.jetissues.data.State
import org.ocpsoft.prettytime.PrettyTime


object IssueUI {
    private val timePrinter = PrettyTime()

    private val ISSUE_DATE_STYLE = TextStyle(color = Color.Gray, fontStyle = FontStyle.Italic)


    @Composable
    fun ListItem(effect: EffectHandler, x: Issue) {
        Ripple(bounded = true) {
            Clickable(onClick = { effect(Event.SelectIssue(x)) }) {
                Card(modifier = LayoutWidth.Fill + LayoutPadding(10.dp)) {
                    CardBody(x)
                }
            }
        }

    }

    @Composable
    private fun CardBody(x: Issue) {
        Row(modifier = LayoutWidth.Fill, arrangement = Arrangement.SpaceBetween) {
            Column {
                Row {
                    CreatedAt(x)
                    Spacer(LayoutWidth(10.dp))
                    Number(x)
                }

                Title(x)
                Labels(x.github.labels)
            }
        }
    }

    @Composable
    private fun CreatedAt(x: Issue) {
        Text(text = timePrinter.format(x.github.createdAt), style = ISSUE_DATE_STYLE)
    }

    @Composable
    private fun Title(x: Issue) {
        Text(text = x.github.title)
    }

    @Composable
    private fun Number(x: Issue) {
        Text(text = "#${x.github.number}")
    }

    @Composable
    private fun Labels(labels: IssuesQuery.Labels?) {
        Row {
            labels?.nodes?.filterNotNull()?.forEach {
                val color = Color(android.graphics.Color.parseColor("#" + it.color))
                val textColor = if (color.luminance() > 0.5) Color.Black else Color.White
                Box(
                    shape = RoundedCornerShape(3.dp),
                    modifier = LayoutPadding(3.dp),
                    backgroundColor = color
                ) {
                    Text(
                        text = it.name,
                        modifier = LayoutPadding(3.dp),
                        style = TextStyle(color = textColor)
                    )
                }
            }
        }
    }

    @Composable
    fun Full(
        effect: EffectHandler,
        x: Issue
    ) {
        Column {
            Scaffold(
                topAppBar = {
                    TopAppBar(
                        title = {
                            Text(
                                text = "#${x.github.number}",
                                style = MaterialTheme.typography().h5
                            )
                        },
                        navigationIcon = {
                            IconImageButton(R.drawable.ic_back) {
                                effect(Event.MainScreen)
                            }
                        }
                    )
                },
                bodyContent = {
                    FullBody(x)
                }
            )
        }
    }

    @Composable
    private fun FullBody(x: Issue) {
        VerticalScroller {
            Column(modifier = LayoutPadding(5.dp)) {
                Text(
                    text = x.github.title,
                    style = MaterialTheme.typography().h5
                )

                Row(arrangement = Arrangement.Center) {
                    CreatedBy(x)
                }

                Labels(x.github.labels)

                Spacer(LayoutHeight(8.dp))

                if (x.error != null) {
                    Error(x.error.toString())
                } else {
                    // TODO markdown rendering
                    x.text?.let {
                        Text(
                            text = it,
                            modifier = LayoutPadding(4.dp),
                            style = MaterialTheme.typography().body1
                        )
                    } ?: run {
                        Loader()
                    }
                }
            }
        }
    }

    @Composable
    private fun CreatedBy(x: Issue) {
        val text = AnnotatedString.Builder().apply {
            pushStyle(ISSUE_DATE_STYLE.toSpanStyle())
            append(timePrinter.format(x.github.createdAt))
            popStyle()
            x.github.author?.login?.let {
                append(" by ")
                pushStyle(SpanStyle(fontWeight = FontWeight.Bold))
                append(it)
            }
        }.toAnnotatedString()
        Text(text = text)
    }

    @Composable
    fun ListBody(
        state: State,
        effect: EffectHandler,
        scroll: ScrollerPosition
    ) {
        VerticalScroller(scrollerPosition = scroll) {
            Column {
                for (iss in state.issues) {
                    ListItem(effect, iss)
                }

                if (state.error != null) {
                    Error(state.error.toString())
                } else if (state.loading) {
                    Loader()
                } else {
                    MoreButton(state)
                }
            }
        }
    }

    @Composable
    private fun MoreButton(state: State) {
        state.loadNext?.let {
            Box(
                gravity = Alignment.Center,
                modifier = LayoutWidth.Fill + LayoutPadding(10.dp)
            ) {
                Button(onClick = it) {
                    Text(text = "More")
                }
            }
        }
    }

    @Composable
    fun List(state: State, effect: EffectHandler) {
        val scroll = ScrollerPosition()
        Column {
            Scaffold(
                topAppBar = {
                    TopAppBar(
                        title = { Text(text = "JetIssues") },
                        actions = {
                            OrderButton(state, effect, scroll)

                            RefreshButton(effect, scroll)
                        }
                    )
                },
                bodyContent = {
                    Column {
                        FilterTabs(state, effect, scroll)
                        ListBody(state, effect, scroll)
                    }
                }
            )
        }
    }

    @Composable
    private fun RefreshButton(
        effect: EffectHandler,
        scroll: ScrollerPosition
    ) {
        IconImageButton(R.drawable.ic_refresh) {
            effect(Event.Refresh)
            scroll.scrollTo(0F)
        }
    }

    @Composable
    private fun OrderButton(state: State, effect: EffectHandler, scroll: ScrollerPosition) {
        if (state.issuesOrder == IssuesOrder.DESC) {
            IconImageButton(R.drawable.ic_desc) {
                effect(Event.SetIssuesOrder(IssuesOrder.ASC))
                scroll.scrollTo(0F)
            }
        } else {
            IconImageButton(R.drawable.ic_asc) {
                effect(Event.SetIssuesOrder(IssuesOrder.DESC))
                scroll.scrollTo(0F)
            }
        }
    }

    @Composable
    private fun FilterTabs(state: State, effect: EffectHandler, scroll: ScrollerPosition) {
        TabRow(items = IssuesState.values().toList(), selectedIndex = 0) { _, tab ->
            Tab(
                text = { Text(tab.title()) },
                selected = state.issuesState == tab,
                onSelected = {
                    scroll.scrollTo(0F)
                    effect(Event.SetIssuesState(tab))
                })
        }
    }

}