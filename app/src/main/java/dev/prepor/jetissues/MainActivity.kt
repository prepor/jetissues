package dev.prepor.jetissues

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.ui.core.setContent
import dev.prepor.jetissues.data.State

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val state = State()
        val effects = Effects(state)
        effects.schedule(Event.Refresh)
        setContent {
            JetIssuesApp(state) { e -> effects.schedule(e)}
        }
    }

}
